package corejava;

public class Datatypes {
	
	public void testmine()
		{
	System.out.println("Hi");	
	}

	public static void main(String[] args) {
		
	    boolean var1=true;
	    
	    var1=false;
	    
	    char var2,var3='M';
	    		
	    var2=125; //Prints ASCII values
	    
	    byte var4=-128; //Byte range - (-128) to 127
	    
	    short var5=32767; //Short range - (-32768) to 32767
	    
	    int var6=1678909734; //int range - (-2,147,483,648) to 2,147,483,647
	    
	    long var7=8939360418l; //long range - (-9,223,372,036,854,775,808) to 9,223,372,036,854,775,807
	    
	    float var8=12.89f, var9= (float) 323.121439899076743; //float range - (-3.4E+38) to +3.4E+38 - about 7 decimal digits
	    
	    double var10=123438.7667567567467364; //double range - (-1.7E+308) to +1.7E+308 - about 16 decimal digits
	    
	    System.out.println("Variable Declaration and Data Types");
	    
	    System.out.println("Boolean=" +var1);
	    
	    System.out.println("Char1=" +var2);
	    
	    System.out.println("Char2=" +var3);
	    
	    System.out.println("Byte=" +var4);
	    
	    System.out.println("Short=" +var5);
	    
	    System.out.println("int=" +var6);
	    
	    System.out.println("Long=" +var7);
	    
	    System.out.println("Float1=" +var8);
	    
	    System.out.println("Float2=" +var9);
	    
	    System.out.println("Double=" +var10);

	}

}
